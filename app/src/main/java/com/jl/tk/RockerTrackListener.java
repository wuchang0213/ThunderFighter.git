package com.jl.tk;

/**
 * 作者： 吴昶 .
 * 时间: 2018/12/10 14:06
 * 功能简介：
 */
public interface RockerTrackListener {
    void onToLeft();
    void onToRight();
    void onStopMove();
}
