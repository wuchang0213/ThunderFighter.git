package com.jl.tk;

import android.content.Context;
import android.graphics.*;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 作者： 吴昶 .
 * 时间: 2018/12/7 11:19
 * 功能简介：
 */
public class TKView extends View {
    private Paint paint;
    private Random random = new Random();
    private int bombSpeed = 20;
    private int enemySpeed = 10;
    private int plantSpeed = 10;
    private int width;
    private int height;
    private boolean isStart = true;
    private Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.tk);
    private Bitmap bomb = BitmapFactory.decodeResource(getResources(), R.mipmap.bomb);
    private Bitmap enemy = BitmapFactory.decodeResource(getResources(), R.mipmap.enemy);
    private Bitmap blast = BitmapFactory.decodeResource(getResources(), R.mipmap.blast);
    private int tkX;
    private int tkY;
    private boolean isMove = true;
    private float touchX;
    private ArrayList<Point> points = new ArrayList<Point>();
    private ArrayList<Point> enemys = new ArrayList<Point>();//blast
    private ArrayList<Point> blasts = new ArrayList<Point>();
    private int index;
    private int score;
    private TKResultListener tkResultListener;

    public TKView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setColor(Color.parseColor("#ff4358"));
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                handler.sendEmptyMessage(5);
            }
        }, 0, 40);
    }

    public void setTkResultListener(TKResultListener tkResultListener) {
        this.tkResultListener = tkResultListener;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        width = getWidth();
        height = getHeight();

        if (isStart) {
            isStart = false;
            tkX = width / 2;
            tkY = height - 20;
        }

        canvas.drawBitmap(bitmap, tkX - 24, tkY - 50, paint);

        for (int i = 0; i < points.size(); i++) {
            canvas.drawBitmap(bomb, points.get(i).x, points.get(i).y, paint);
        }
        for (int i = 0; i < enemys.size(); i++) {
            canvas.drawBitmap(enemy, enemys.get(i).x - 24, enemys.get(i).y, paint);
        }
        for (int i = 0; i < blasts.size(); i++) {
            canvas.drawBitmap(blast, blasts.get(i).x - 64, blasts.get(i).y, paint);
            blasts.remove(i);
            score++;
            if(tkResultListener!=null){  tkResultListener.onScoreListener(score);  }
            i--;
        }
    }

    /**
     * 放开后 屏幕手势控制
     * 添加了一个操纵杆，可以屏蔽
     */
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                isMove = true;
//                touchX = event.getX();
//                if (touchX > tkX) {
//                    moveToRight();
//                } else {
//                    moveToLeft();
//                }
//                break;
//            case MotionEvent.ACTION_MOVE:
//                touchX = event.getX();
//                break;
//            case MotionEvent.ACTION_UP:
//                isMove = false;
//                break;
//        }
//        return super.onTouchEvent(event);
//    }

    public void stepsLeft(){
        if(!isMove || touchX!=0) {
            handler.removeMessages(1);
            isMove = true;
            touchX = 0;
            handler.sendEmptyMessage(1);
        }
    }

    public void stepsRight(){
        if(!isMove || touchX!=width) {
            handler.removeMessages(1);
            isMove = true;
            touchX = width;
            handler.sendEmptyMessage(1);
        }
    }

    private void moveToRight() {
        if (tkX < width - plantSpeed && tkX < touchX) {
            tkX += plantSpeed;
            invalidate();
        }
    }

    private void moveToLeft() {
        if (tkX > plantSpeed && tkX > touchX) {
            tkX -= plantSpeed;
            invalidate();
        }
    }

    public void stopMove(){
        isMove = false;
    }

    public void continuedMove() {
        handler.sendEmptyMessage(1);
    }

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (touchX > tkX) {
                        moveToRight();
                    } else {
                        moveToLeft();
                    }
                    if (isMove) {
                        handler.sendEmptyMessageDelayed(1, 10);
                    }
                    break;
                case 5:
                    for (int i = 0; i < enemys.size(); i++) {
                        int x = enemys.get(i).x;
                        int y = enemys.get(i).y + enemySpeed;
                        enemys.get(i).x = x;
                        enemys.get(i).y = y;
                        if (enemys.get(i).y >= height) {
                            enemys.remove(i);
                            i--;
                        } else {
                            for (int j = 0; j < points.size(); j++) {
                                Point p = getCollidePoint(x, y, points.get(j).x, points.get(j).y);
                                if (p != null) {
                                    blasts.add(new Point((x + points.get(j).x) / 2, (y + points.get(j).y) / 2));
                                    points.remove(j);
                                    j--;
                                    enemys.remove(i);
                                    i--;
                                    break;
                                }
                            }
                        }
                    }
                    for (int i = 0; i < points.size(); i++) {
                        Point p = points.get(i);
                        points.get(i).x = p.x;
                        points.get(i).y = p.y - bombSpeed;
                        if (points.get(i).y <= 0) {
                            points.remove(i);
                            i--;
                        }
                    }
                    index++;
                    if (index % 5 == 0) {
                        points.add(new Point(tkX, (tkY - 60)));
                    }
                    if (index % 15 == 0) {
                        int x = random.nextInt(width);
                        enemys.add(new Point(x, 0));
                    }
                    invalidate();
                    break;
            }
            return false;
        }
    });

    public Point getCollidePoint(int x, int y, int tx, int ty) {
        //碰撞点初始为0
        Point p = null;
        //得到第一个碰撞精灵位图的RectF类
        RectF rectF1 = new RectF(x, y, x + 24f, y + 24f);
        //得到第二个碰撞精灵位图的RectF类
        RectF rectF2 = new RectF(tx, ty, tx + 24f, ty + 24f);
        //新的RectF
        RectF rectF = new RectF();
        //通过setIntersect()方法得到两精灵是否相交的布尔值
        boolean isIntersect = rectF.setIntersect(rectF1, rectF2);
        //如果两精灵相交
        if (isIntersect) {
            //得到交点
            p = new Point(Math.round(rectF.centerX()), Math.round(rectF.centerY()));
        }
        //返回交点
        return p;
    }


}
