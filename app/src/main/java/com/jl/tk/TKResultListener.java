package com.jl.tk;

/**
 * 作者： 吴昶 .
 * 时间: 2018/12/10 10:07
 * 功能简介：
 */
public interface TKResultListener {
    void onScoreListener(int score);
    void onGameOver();
}
