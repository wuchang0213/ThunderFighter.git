package com.jl.tk

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),TKResultListener,RockerTrackListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tk_01.setOnClickListener { tk_01.invalidate() }
        tk_01.setTkResultListener(this)
        rkv_rocker.setTrackListener(this)

        tk_01.setOnLongClickListener {
            tk_01.continuedMove()
            true
        }
        rkv_rocker.setOnClickListener { rkv_rocker.invalidate() }
    }

    override fun onScoreListener(score: Int) {
        tv_score.text="$score"
    }

    override fun onGameOver() {

    }

    override fun onToLeft() {
        tk_01.stepsLeft()
    }

    override fun onToRight() {
        tk_01.stepsRight()
    }

    override fun onStopMove() {
        tk_01.stopMove()
    }
}
